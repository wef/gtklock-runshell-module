// gtklock-runshell-module
// Copyright (c) 2022-23 Erik Reider, Jovan Lanik, Bob Hepple

// CSS text module

#include "gtklock-module.h"

#define MODULE_DATA(x) (x->module_data[self_id])
#define RUNSHELL(x) ((struct runshell *)MODULE_DATA(x))

extern void config_load(const char *path, const char *group, GOptionEntry entries[]);

struct runshell {
	GtkWidget *runshell_revealer;
	GtkWidget *runshell_box;
	GtkWidget *runshell_label;
    GtkWidget *timer_label;
};

const gchar module_name[] = "runshell";
const guint module_major_version = 4;
const guint module_minor_version = 0;

static int self_id;

static gchar *command = NULL;
static int refresh_time = 0;
static gchar *position = NULL;
static gchar *justify = NULL;
static int margin_top = 0;
static int margin_bottom = 0;
static int margin_left = 0;
static int margin_right = 0;

GOptionEntry module_entries[] = {
	{ "command", 0, 0, G_OPTION_ARG_STRING, &command, "Shell command to run", NULL },
	{ "refresh", 0, 0, G_OPTION_ARG_INT, &refresh_time, "How often to refresh (secs). Default 0=no refresh", NULL },
    { "runshell-position", 0, 0, G_OPTION_ARG_STRING, &position, "Position of text (top-left, bottom-center, etc)", NULL },
    { "justify", 0, 0, G_OPTION_ARG_STRING, &justify, "Justify the text (left or right)", NULL },
    { "margin-top", 0, 0, G_OPTION_ARG_INT, &margin_top, "Top margin", NULL },
    { "margin-bottom", 0, 0, G_OPTION_ARG_INT, &margin_bottom, "Bottom margin", NULL },
    { "margin-left", 0, 0, G_OPTION_ARG_INT, &margin_left, "Left margin", NULL },
    { "margin-right", 0, 0, G_OPTION_ARG_INT, &margin_right, "Right margin", NULL },
	{ NULL },
};

gchar* run_shell_command(const gchar* command) {
    gchar* output = NULL;
    GError* error = NULL;

    if (!g_spawn_command_line_sync(command, &output, NULL, NULL, &error)) {
        g_print("Error running command: %s\n", error->message);
        g_clear_error(&error);
        return NULL;
    }

    return output;
}

static void window_set_runshell(gchar *cmd, struct Window *ctx) {
	if (cmd == NULL) {
		g_warning("runshell-module: no command found");
		gtk_container_remove(GTK_CONTAINER(ctx->window_box), RUNSHELL(ctx)->runshell_label);
	} else {
        gchar *text = NULL;
        text = run_shell_command(cmd);
        gtk_label_set_markup(GTK_LABEL(RUNSHELL(ctx)->runshell_label), text);
        g_free(text);
    }
}

gboolean refresh_callback(gpointer data)
{
    struct Window *ctx = (struct Window *)data;
    
    window_set_runshell(command, ctx);
            
    // Return TRUE to continue calling the function
    return TRUE;
}

static void setup_runshell(struct Window *ctx) {
	if(MODULE_DATA(ctx) != NULL) {
		gtk_widget_destroy(RUNSHELL(ctx)->runshell_revealer);
		g_free(MODULE_DATA(ctx));
		MODULE_DATA(ctx) = NULL;
	}
	MODULE_DATA(ctx) = g_malloc(sizeof(struct runshell));

	RUNSHELL(ctx)->runshell_revealer = gtk_revealer_new();
	gtk_widget_set_halign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_CENTER);
	gtk_widget_set_name(RUNSHELL(ctx)->runshell_revealer, "runshell-revealer");
	gtk_revealer_set_reveal_child(GTK_REVEALER(RUNSHELL(ctx)->runshell_revealer), TRUE);
	gtk_revealer_set_transition_type(GTK_REVEALER(RUNSHELL(ctx)->runshell_revealer), GTK_REVEALER_TRANSITION_TYPE_NONE);
	gtk_overlay_add_overlay(GTK_OVERLAY(ctx->overlay), RUNSHELL(ctx)->runshell_revealer);

    if(
        g_strcmp0(position, "top-left") == 0 ||
        g_strcmp0(position, "center-left") == 0 ||
        g_strcmp0(position, "bottom-left") == 0
        ) gtk_widget_set_halign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_START);
    else if(
        g_strcmp0(position, "top-right") == 0 ||
        g_strcmp0(position, "center-right") == 0 ||
        g_strcmp0(position, "bottom-right") == 0

        ) gtk_widget_set_halign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_END);
    else gtk_widget_set_halign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_CENTER);

    if(
        g_strcmp0(position, "top-left") == 0 ||
        g_strcmp0(position, "top-right") == 0 ||
        g_strcmp0(position, "top-center") == 0
        ) gtk_widget_set_valign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_START);
    else if(
        g_strcmp0(position, "bottom-left") == 0 ||
        g_strcmp0(position, "bottom-right") == 0 ||
        g_strcmp0(position, "bottom-center") == 0
        ) gtk_widget_set_valign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_END);
    else gtk_widget_set_valign(RUNSHELL(ctx)->runshell_revealer, GTK_ALIGN_CENTER);

    gtk_widget_set_margin_start(RUNSHELL(ctx)->runshell_revealer, margin_left);
    gtk_widget_set_margin_end(RUNSHELL(ctx)->runshell_revealer, margin_right);
    gtk_widget_set_margin_top(RUNSHELL(ctx)->runshell_revealer, margin_top);
    gtk_widget_set_margin_bottom(RUNSHELL(ctx)->runshell_revealer, margin_bottom);
                              
	RUNSHELL(ctx)->runshell_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_widget_set_halign(RUNSHELL(ctx)->runshell_box, GTK_ALIGN_CENTER);
	gtk_widget_set_name(RUNSHELL(ctx)->runshell_box, "runshell-box");
	gtk_container_add(GTK_CONTAINER(RUNSHELL(ctx)->runshell_revealer), RUNSHELL(ctx)->runshell_box);

	RUNSHELL(ctx)->runshell_label = gtk_label_new(NULL);
	gtk_widget_set_name(RUNSHELL(ctx)->runshell_label, "runshell");
	g_object_set(RUNSHELL(ctx)->runshell_label, "margin-bottom", 10, NULL);
	gtk_container_add(GTK_CONTAINER(RUNSHELL(ctx)->runshell_box), RUNSHELL(ctx)->runshell_label);

    if (g_strcmp0(justify, "right") == 0) 
        gtk_label_set_justify(GTK_LABEL(RUNSHELL(ctx)->runshell_label), GTK_JUSTIFY_RIGHT);
    else if (g_strcmp0(justify, "center") == 0) 
        gtk_label_set_justify(GTK_LABEL(RUNSHELL(ctx)->runshell_label), GTK_JUSTIFY_CENTER);
    else if (g_strcmp0(justify, "fill") == 0) 
        gtk_label_set_justify(GTK_LABEL(RUNSHELL(ctx)->runshell_label), GTK_JUSTIFY_FILL);

	RUNSHELL(ctx)->timer_label = gtk_label_new(NULL);
	gtk_widget_set_name(RUNSHELL(ctx)->timer_label, "timer");
	g_object_set(RUNSHELL(ctx)->timer_label, "margin-bottom", 10, NULL);
	gtk_container_add(GTK_CONTAINER(RUNSHELL(ctx)->runshell_box), RUNSHELL(ctx)->timer_label);

	window_set_runshell(command, ctx);
	gtk_widget_show_all(RUNSHELL(ctx)->runshell_revealer);

    // Set up refresh
    if (refresh_time > 0) {
        g_timeout_add_seconds(refresh_time, refresh_callback, (gpointer)ctx);
    }
}

void g_module_unload(GModule *m) {
	// g_object_unref(runshell); // gives seg.viol.
    g_free(command);
    g_free(position);
    g_free(justify);
}

void on_activation(struct GtkLock *gtklock, int id) {
	self_id = id;

	GtkCssProvider *provider = gtk_css_provider_new();
	GError *err = NULL;
	const char css[] =
		"#runshell {"
		"font-size: 18pt;"
		"}"
		;

	gtk_css_provider_load_from_data(provider, css, -1, &err);
	if(err != NULL) {
		g_warning("Style loading failed: %s", err->message);
		g_error_free(err);
	} else {
		gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
			GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	}

	g_object_unref(provider);
}

void on_focus_change(struct GtkLock *gtklock, struct Window *win, struct Window *old) {
	setup_runshell(win);
	if(gtklock->hidden)
		gtk_revealer_set_reveal_child(GTK_REVEALER(RUNSHELL(win)->runshell_revealer), FALSE);
	if(old != NULL && win != old)
		gtk_revealer_set_reveal_child(GTK_REVEALER(RUNSHELL(old)->runshell_revealer), FALSE);
}

void on_window_destroy(struct GtkLock *gtklock, struct Window *ctx) {
	if(MODULE_DATA(ctx) != NULL) {
		g_free(MODULE_DATA(ctx));
		MODULE_DATA(ctx) = NULL;
	}
}

void on_idle_hide(struct GtkLock *gtklock) {
	if(gtklock->focused_window) {
		GtkRevealer *revealer = GTK_REVEALER(RUNSHELL(gtklock->focused_window)->runshell_revealer);	
		gtk_revealer_set_reveal_child(revealer, FALSE);
	}
}

void on_idle_show(struct GtkLock *gtklock) {
	if(gtklock->focused_window) {
		GtkRevealer *revealer = GTK_REVEALER(RUNSHELL(gtklock->focused_window)->runshell_revealer);	
		gtk_revealer_set_reveal_child(revealer, TRUE);
	}
}

